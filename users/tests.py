from django.test import TestCase
from .models import CustomUser

class UserTest(TestCase):
	def setUp(self):
		CustomUser.objects.create(first_name='kevin', last_name='asyraf')
		CustomUser.objects.create(username='kevin.asyraf')

	def test_correct_first_name(self):
		kevin = CustomUser.objects.get(first_name='kevin')
		self.assertEqual(kevin.first_name, 'kevin')

	def test_correct_last_name(self):
		asyraf = CustomUser.objects.get(last_name='asyraf')
		self.assertEqual(asyraf.last_name, 'asyraf')

	def test_correct_username(self):
		trap = CustomUser.objects.get(username='kevin.asyraf')
		self.assertEqual(trap.username, 'kevin.asyraf')



