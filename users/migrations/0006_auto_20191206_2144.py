# Generated by Django 2.0.6 on 2019-12-06 14:44

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0005_auto_20191206_1534'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='category',
            name='book',
        ),
        migrations.RemoveField(
            model_name='customuser',
            name='preferred_category',
        ),
        migrations.DeleteModel(
            name='Category',
        ),
    ]
