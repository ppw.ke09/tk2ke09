from django.urls import path, include
from . import views

urlpatterns = [
   path('', views.list_buku, name='list_buku'),
   path('getData', views.get_data),
]